//
//  main.m
//  TestApp
//
//  Created by Logan Buchanan on 3/3/14.
//  Copyright (c) 2014 FortyAU. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
