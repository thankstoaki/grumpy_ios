//
//  AppDelegate.h
//  TestApp
//
//  Created by Logan Buchanan on 3/3/14.
//  Copyright (c) 2014 FortyAU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
